package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import database.SQLiteConnection;

public class Spider {

	
    private Set<String> articles; //List of links to articles
    private List<String> categories;
    private SQLiteConnection con;
    boolean fullSearch; //decides whether we navigate through the whole blog to find article links
    
    public enum Month {
    	Januar ("01"),
    	Februar ("02"),
    	M�rz ("03"),
    	April ("04"),
    	Mai ("05"),
    	Juni ("06"),
    	Juli ("07"),
    	August ("08"),
    	September ("09"),
    	Oktober ("10"),
    	November ("11"),
    	Dezember ("12");
    	
    	private String numMonth;
    	
    	Month(String numMonth) {
    		this.numMonth = numMonth;
    	}
    	
    	String getNumMonth() {
    		return numMonth;
    	}
    }
    public static void main(String[] args) {
    	
    	String URL = "http://meerblog.de/";
    	Spider spidey = new Spider("crawler.db");
    	
    	long then = System.currentTimeMillis();
    	try {

    		//main crawler
    		spidey.master(URL);
    		System.out.println("Full search: " + spidey.fullSearch);
    		spidey.articleDataWriter();

    		//n-Grams
    		/*String articleText = spidey.articleTextFetcher("http://meerblog.de/expedition-antaktis-tagebuch-7-cuverville-island/");
    		List<String> stopWordFreeText= spidey.stopwordRemover(articleText);
    		List<List<String>> nGramList = spidey.nGramFinder(2, stopWordFreeText);
    		Map<String, Integer> map = spidey.nGramFreq(2, nGramList);
    		System.out.println(map);*/
    		
    		//Testing WDFIDF
    		/*List<String> korpus = new ArrayList<String>();
				String docURL = "http://meerblog.de/expedition-antaktis-tagebuch-7-cuverville-island/";
				spidey.master(URL);
				System.out.println("Fetching article page text");
				for(String article: spidey.articles) {

					korpus.add(spidey.pageTextFetcher(article));
				}
				System.out.println("Done fetching article page text");
				System.out.println("articles size: " + spidey.articles.size());
				double wdfidf = spidey.WDFIDF(korpus, spidey.pageTextFetcher(docURL), "abschied");
				System.out.println(wdfidf);*/
    		
    		//Keyword-Density
    		/*String text = spidey.articleTextFetcher("http://meerblog.de/expedition-antaktis-tagebuch-7-cuverville-island/");
    		double kwd = spidey.keywordDensity(text, "schiff");
    		System.out.println("Keyword density = " + kwd);*/
    		
    		long now = System.currentTimeMillis();
    		System.out.println("Time elapsed in milliseconds: " + (now-then));
    		System.out.println("Time elapsed in minutes: " + (now-then)/60000);
    	} catch(IOException e) {
    		System.out.println(e.getClass().getSimpleName());
    		e.printStackTrace();
    		System.out.println("Please run program again...");
    	}
    }
	
	
	public Spider(String DBpath) {
		
		//pagesVisited = new HashSet<String>();
	    //pagesToVisit = new ArrayList<String>();
	    articles = new HashSet<String>();
	    categories = new ArrayList<String>();
	    con = new SQLiteConnection(DBpath);
	    fullSearch = true;
	    
	}
	/**
	 * Retrieves URLs of all categories on a page and adds them to pagesToVisit
	 * @param URL
	 * @throws IOException
	 */
	public void categoryFetcher(String URL) throws IOException{

		System.out.println("Fetching categories");
		//connect to page (Preferably home)
		Document doc = Jsoup.connect(URL).timeout(10*1000).get();


		//get all categories and add them to pagesToVisit
		Elements links = doc.select("a[href]");
		for(Element link: links){
			if(link.attr("href").contains("/category/")) {
				//System.out.println(link.attr("href"));
				//pagesToVisit.add(link.attr("href"));
				categories.add(link.attr("href"));
			}
		}
		System.out.println("Done fetching categories");
	}
	
	/**
	 * Searches the DB for available article links. Gets any new article links by searching the front page
	 * of categories. Retrieves URLs of all articles in all categories and adds them to articles list
	 * if the DB is empty.
	 * 
	 * @param URL Link of category page
	 * @throws IOException
	 */
	public void articleFetcher(String URL) throws IOException {

		//List<String> prevArtikelList = new ArrayList<String>(); // List containing links to previous pages
		
		/*
		 * Search in the DB for available article links to skip having to search the whole blog everytime.
		 * To get new articles it will suffice to check the front pages of categories.
		 * If DB is empty, search the whole blog as normal.
		 */
		
		if(!fullSearch) {
			
			Document doc = Jsoup.connect(URL).timeout(10*1000).get();
			
			//get all articles in each page and add them to article
			Elements articles = doc.select("article"); //search in document for elements containing "article"
			for(Element article: articles) { //for every article element...
				Elements members = article.getElementsByTag("h2"); //search for elements with tag 'h2' i.e. headers
				for(Element member: members) {	//for every such element...
					//System.out.println(member.tagName());
					try {
						Element articleLink = member.child(0);	//get it's first child (should be 'a' element i.e. contains our link)
						//System.out.println(articleLink.attr("href"));
						//pagesToVisit.add(articleLink.attr("href")); 
						this.articles.add(articleLink.attr("href"));
					} catch(IndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}
		}
		else {
			
			//connect to page (Preferably a category)
			Document doc = Jsoup.connect(URL).timeout(10*1000).get();


			//search for <span> elements with text "vorherige Artikel" and get their link i.e. the <a href> tag
			Elements spans = doc.select("span"); //search in document for elements containing "span"
			for(Element span: spans) {
				if(span.hasText() && span.text().equals("Vorherige Artikel")) {
					//prevArtikelList.add(span.parent().attr("href"));

					// call articleFetcher recurively with the link to the previous page as an argument
					// span.parent().attr("href") is (or should be :D ) the link to the previous page.
					articleFetcher(span.parent().attr("href")); 
					break;
				}
			}

			//get all articles in each page and add them to pagesToVisit and article
			Elements articles = doc.select("article"); //search in document for elements containing "article"
			for(Element article: articles) { //for every article element...
				Elements members = article.getElementsByTag("h2"); //search for elements with tag 'h2' i.e. headers
				for(Element member: members) {	//for every such element...
					//System.out.println(member.tagName());
					try {
						Element articleLink = member.child(0);	//get it's first child (should be 'a' element i.e. contains our link)
						//System.out.println(articleLink.attr("href"));
						//pagesToVisit.add(articleLink.attr("href")); 
						this.articles.add(articleLink.attr("href"));
					} catch(IndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}
		}
		//System.out.println(prevArtikelList.size() + ": " + prevArtikelList);
	}
	
	/**
	 * The master spider. Calls all other methods in a sensible order. 
	 * Should be the only method called when the program is run.
	 * After calling this method, the articles Set contains the links of all articles on the blog.
	 * @param home Blog's home page
	 * @throws IOException
	 */
	public void master(String home) throws IOException {
		
		Connection c = con.initiate();

		/*
		 * If DB is empty, a full search of the blog should be done.
		 * If not, use links from the DB and get any new article links from the first page of categories.
		 */
		try {
			Statement stmt = c.createStatement();
			//should get articles from entries with highest iteration instead v
			//ResultSet rs = stmt.executeQuery("SELECT * FROM articles WHERE iteration=1;"); 
			ResultSet rs = stmt.executeQuery("SELECT * FROM articles;"); 
			/*while(rs.next()) {
				fullSearch = false;	
				articles.add(rs.getString("link"));
			}*/
			if(rs.next()) {
				rs = stmt.executeQuery("SELECT max(iteration) FROM articles;");
				if(rs.next()) {
					Integer largestIteration = rs.getInt(1);		
					rs = stmt.executeQuery("SELECT * FROM articles WHERE iteration="+largestIteration.toString()+";");
					while(rs.next()) {
						fullSearch = false;	
						articles.add(rs.getString("link"));
					}
				}
			}
			rs.close();
			stmt.close();
			c.close();
			System.out.println("Database closed");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//System.out.println(articles.size());
		categoryFetcher(home);
		//This for-loop serves only to speed up the process for testing purposes by using articles from a
		//category which, I believe, contains fewer articles. In reality, all articles from all categories
		//should be taken into consideration.
		System.out.println("Fetching articles");
		for(String category: categories) {
			/*if(category.contains("antarktis")) {
				articleFetcher(category);
				break;
			}*/
			articleFetcher(category);
		}
		System.out.println("done fetching!");
		//articleDataWriter();
	}
	
	/**
	 * Retrieves the data we need from articles (i.e. num. of comments, num. of shares etc.) and writes them 
	 * to the database. 
	 * @throws IOException 
	 */
	public void articleDataWriter() throws IOException {
		
		Set<String> pagesVisited = new HashSet<String>();
		//Giving meaningless initial values to avoid "local variable may not have been initialized"
		String name = ""; 
		String category = "";
		String link = ""; 
		String releaseDateString = "";
		LocalDate releaseDate;
		LocalDate dateCollected;
		int comments = 0; 
		int shares = 0;
		int iteration = 0;
		
		//Establish DB connection
		Connection c = con.initiate();
		try {
			//Set iteration
			//If article already exists in DB, this entry's iteration will equal to the 
			//largest iteration plus 1 
			Statement stmt = c.createStatement();

			//We do * first since asking for MAX returns something even if table is empty.
			//this gives us a chance to know if table is empty. 
			ResultSet rs = stmt.executeQuery("SELECT * FROM articles;"); 
			int largestIteration = 0;
			while(rs.next()) {
				ResultSet rs2 = stmt.executeQuery("SELECT MAX(iteration) FROM articles;");
				if(rs2.next()) {
					largestIteration = rs2.getInt(1);		
					rs2.close();
					break;
				}
			}
			iteration = ++largestIteration; 

			rs.close();
			stmt.close();
		} catch(SQLException e) {
			e.printStackTrace();
			throw new IOException(); // just something to make us land in main's catch block. I want the user to restart the program whenever an exception occurs.
		}
		
		for(String article: articles) {
			
			
			//Checking if article has been visited before and skips an iteration if it has.
			//If not adds article link to list of visited articles so as to not visit it again.
			 
			if(pagesVisited.contains(article))
				continue;
			else
				pagesVisited.add(article);
			
			try {
				//connect to article page
				Document doc = Jsoup.connect(article).timeout(10*1000).get();
				
				//Retrieve name
				Elements h1s = doc.select("h1");	//search in document for h1 elements
				for(Element h1: h1s) {
					if(h1.hasAttr("class") && h1.attr("class").equals("entry-title")) {
						//found article title header. Start parsing for title/name
						
						name = h1.text();
						//System.out.println(name);
						break;
					}
				}		
				
				//Retrieve category			
				category = "";
				Elements as = doc.select("a");	//search in document for a elements
				
				for(Element a: as) {
					
					if(a.hasAttr("rel") && a.attr("rel").equals("category tag")) {
						//found category-/ies tag. Parse for categor-/ies
						
						if(category.length() != 0)
							category = category + ", ";
						
						category = category + a.text();						
					}
				}
				//System.out.println(category);				
				
				
				//Retrieve link			
				link = article;
				
				//Retrieve releaseDateString and change date format to make it SQLite compatible
				String month = "";
				for(Element a: as) {
					
					if(a.hasAttr("href") && a.attr("href").equals(link)) {
						String s = a.text();
						for(Month m: Month.values()) {
							//System.out.println(m.toString());
							if(s.contains(m.toString())) {
								releaseDateString = s.replaceAll(m.toString(), m.getNumMonth()); //replace month with its numeric value
								month = m.getNumMonth();
							}							
						}
						break;
					}
				}
				String day = "";
				char[] releaseDateCharArr = releaseDateString.toCharArray();
				for(int i = 0; i< releaseDateString.length(); i++) {
					try {
						Integer.parseInt(Character.toString(releaseDateCharArr[i]));
						day = day + Character.toString(releaseDateCharArr[i]); 
					} catch(NumberFormatException e) {
						//System.out.println("not a number");
						break;
					}
				}
				if(day.length() < 2) {
					day = "0" + day;
				}
				String year = releaseDateString.substring(releaseDateString.length() - 4, releaseDateString.length());
				releaseDateString = "";
				releaseDateString = year + "-" + month + "-" + day;	
				DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				try {
					releaseDate = LocalDate.parse(releaseDateString, dateFormat);
				} catch(DateTimeParseException e) {
					e.getStackTrace();
					releaseDate = LocalDate.parse(releaseDateString, dateFormat);
				}
				//System.out.println(releaseDate);
				
				//Get dateCollected
				dateCollected = LocalDate.now();
				//System.out.println(dateCollected);
				
				//Retrieve num of comments
				Elements h3s = doc.select("h3"); //search in document for h3 elements
				for(Element h3: h3s) {
					if(h3.hasAttr("class") && h3.attr("class").equals("comments-title")) {
						//found comment header. Start parsing for number of comments
						String s = h3.text();
						comments = Integer.parseInt(s.substring(0, s.indexOf(" ")));
						break;
					}
				}				
				
				//Retrieve num of shares
				Elements scripts = doc.select("script");

				for(Element script: scripts) {
					CharSequence cs = "\"shares\":\"";
					
					if(script.hasAttr("type") && script.attr("type").equals("text/javascript") 
							&& script.toString().contains(cs)) {
						//found our script tag. Parse for number of shares
						String s = script.toString();
						int lenBeg = "\"shares\":\"".length();	//length of the string "shares":"
						int beg = s.indexOf("\"shares\":\"");	//start index of "shares":" in s
						int numBeg = lenBeg + beg;	//end index of "shares":" in s | aka num start
						int numEnd = s.indexOf("\",\"round_shares");	//start index of ","round_shares" in s | aka num end
						String num = s.substring(numBeg, numEnd);
						shares = Integer.parseInt(num);
						break;
					}
				}
				
				//write article data to DB.
				Statement stmt = c.createStatement();
				stmt.executeUpdate("INSERT INTO articles (name, category, link, releaseDate, "
						+ "dateCollected, comments, shares, iteration)"
						+ " VALUES ("+"\""+name+"\""+","+"\""+category+"\""+","+"\""+link+"\""+","+"\""+releaseDate+"\""+
						","+"\""+dateCollected+"\""+","+comments+","+shares+","+iteration+");");
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new IOException(); // just something to make us land in main's catch block. I want the user to restart the program whenever an exception occurs.
			}
		}
		try {
			c.commit();
			c.close();	//close DB connection
			System.out.println("Database closed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException(); // just something to make us land in main's catch block. I want the user to restart the program whenever an exception occurs.
		}
	}
	
	/**
	 * Returns an article's text.
	 * @param articleLink The web link to an article
	 * @return The article's entire text as a String
	 * @throws IOException
	 */
	public String articleTextFetcher(String articleLink) throws IOException {
		
		String text = "";
		
		Document doc = Jsoup.connect(articleLink).timeout(10*1000).get();
		
		//get all paragraphs in the article
		Elements divs = doc.select("div");
		for(Element div: divs) {
			if(div.hasAttr("class") && div.attr("class").equals("entry-content")) {
				Elements paragraphs = div.children();
				for(Element paragraph: paragraphs) { //for every paragraph element...
					
					if(paragraph.tagName().equals("p") && !paragraph.hasAttr("class")) {
						if(text.length() != 0)
							text = text + " ";
						text = text + paragraph.text();
					}			
				}
				break;
			}
		}
		return text;
	}
	
	/**
	 * Parses a web page for all it's text. html tags are disregarded and only their 
	 * VISIBLE content is included. 
	 * @param URL A web page's url.
	 * @return A string containing the collected text. 
	 * @throws IOException 
	 */
	public String pageTextFetcher(String URL) throws IOException {
		
		Document doc = Jsoup.connect(URL).timeout(10*1000).get();
		Elements bodys = doc.select("body");
		String text = "";
		
		
		for(Element body: bodys) {
			
			Elements children = body.children();
			
			for(Element child: children) {
				
				if(!child.tagName().equals("script")) {
					
					if(child.hasText()) {
						
						text = text + child.text();
					}
				}
			}
		}
		
		//System.out.println(text);
		return text;
	}
	
	/**
	 * Returns the keyword density for a specific keyword in a specific text.
	 * Stop words are removed first.
	 * Formula used: (num. of occurences in text/num. of words in text) * 100 
	 * Case insensitive!
	 * @param text The text to be analyzed
	 * @param keyword Word being searched for
	 * @return keyword density
	 */
	public double keywordDensity(String text, String keyword) {
		
		List<String> textAsList = stopwordRemover(text);
		//double kwd;
		int occ = 0;
		System.out.println(textAsList);
		for(String word: textAsList) {
			
			if(word.toLowerCase().equals(keyword.toLowerCase()))
				++occ;
		}
		
		return (((double)occ/(double)textAsList.size()) * 100);		
	}
	
	/**
	 * Performs a WDF*IDF for a keyword on a number of texts.
	 * @param korpus A list containing the texts forming the Collection used for the IDF calculation
	 * @param keyword The keyword
	 * @param doc The document in question
	 * @return The calculated frequency
	 */
	public double WDFIDF(List<String> korpus, String doc, String keyword) {
		
		List<String> textAsList = stopwordRemover(doc);
		List<List<String>> matchedText = new ArrayList<List<String>>(); // Texts in the Korpus where a keyword match was found will be added here.
		int occ = 0;
		double wdf;
		double idf;
		System.out.println("WDFIDF analysis started");
		//Calculating wdf for the document
		for(String word: textAsList) {
			
			if(word.toLowerCase().equals(keyword.toLowerCase()))
				++occ;
		}
		wdf = logCalc(occ + 1, 2) / logCalc(textAsList.size(), 2);
		
		occ = 0;
		
		//calculating idf
		for(String text: korpus) {
			
			textAsList = stopwordRemover(text);
			for(String word: textAsList) {
				
				if(word.toLowerCase().equals(keyword.toLowerCase())){ 
				
					matchedText.add(textAsList);
					break;
				}
			}
		}
		System.out.println("WDFIDF analysis finished");
		System.out.println("matchedText size: " + matchedText.size());
		idf = Math.log10((double)korpus.size() / (double)matchedText.size());
		
		return wdf*idf;
	}
	
	/**
	 * Starts by removing all punctuation from a text.Then, removes stop words from the text.
	 * @param text Text whose stop words are to be removed
	 * @return A List containing only those words from the text which aren't stop words. 
	 */
	public List<String> stopwordRemover(String text) {
		
		//List<String> stopList = new ArrayList<String>();
		String [] stopList = {"ich", "du", "er", "sie", "es", "ihr", "wir", 
				"seins", "seines", "seiner", "seine", "seinen", "seinem",
				"ihres", "ihren", "ihrem", "ihre", "ihrer", "ihrer", "ihrs",
				"mein", "meins", "meines", "meiner", "meine", "meinen", "meinem", 
				"mich", "mir", "dich", "dir", "ihn", "ihm", "euch", "ihnen", "uns",
				"deines", "deins", "deiner", "deine", "deinen", "deinem", "dein",  
				"eure", "euren", "eurem", "eurer", "unsers", "unseres", "unserer", "eures", "euer",  
				"ein", "eine", "einen", "einem", "eines", "einer", "unserem", "unsere", 
				"der", "die", "das", "den", "dem", "des", "sein", "unser", "unseren",  
				"f�r", "bis", "durch", "ohne", "gegen", "au�erhalb", "innerhalb", 
				"ab", "an", "am", "um", "bei", "beim", "von", "vom", "seit", "mit", "gegen�ber",
				"aus", "zu", "zur", "zum", "in", "im", "unter", "vor", "�ber", "hinter", "auf", "zwischen",
				"neben", "&", "und"};
		//String [] stopList = {"ab", "bei", "da", "deshalb", "ein", "f�r", "finde", "haben", "hier", "ich", "ja", "kann", "machen", "muesste", "nach", "oder", "seid", "sonst", "und", "vom", "wann", "wenn", "wie", "zu", "bin", "eines", "hat", "manche", "solches", "an", "anderm", "bis", "das", "deinem", "demselben", "dir", "doch", "einig", "er", "eurer", "hatte", "ihnen", "ihre", "ins", "jenen", "keinen", "manchem", "meinen", "nichts", "seine", "soll", "unserm", "welche", "werden", "wollte", "w�hrend", "alle", "allem", "allen", "aller", "alles", "als", "also", "am", "ander", "andere", "anderem", "anderen", "anderer", "anderes", "andern", "anders", "auch", "auf", "aus", "bist", "bsp.", "daher", "damit", "dann", "dasselbe", "dazu", "da�", "dein", "deine", "deinen", "deiner", "deines", "dem", "den", "denn", "denselben", "der", "derer", "derselbe", "derselben", "des", "desselben", "dessen", "dich", "die", "dies", "diese", "dieselbe", "dieselben", "diesem", "diesen", "dieser", "dieses", "dort", "du", "durch", "eine", "einem", "einen", "einer", "einige", "einigem", "einigen", "einiger", "einiges", "einmal", "es", "etwas", "euch", "euer", "eure", "eurem", "euren", "eures", "ganz", "ganze", "ganzen", "ganzer", "ganzes", "gegen", "gemacht", "gesagt", "gesehen", "gewesen", "gewollt", "hab", "habe", "hatten", "hin", "hinter", "ihm", "ihn", "ihr", "ihrem", "ihren", "ihrer", "ihres", "im", "in", "indem", "ist", "jede", "jedem", "jeden", "jeder", "jedes", "jene", "jenem", "jener", "jenes", "jetzt", "kein", "keine", "keinem", "keiner", "keines", "konnte", "k�nnten", "k�nnen", "k�nnte", "mache", "machst", "macht", "machte", "machten", "man", "manchen", "mancher", "manches", "mein", "meine", "meinem", "meiner", "meines", "mich", "mir", "mit", "muss", "musste", "m��t", "nicht", "noch", "nun", "nur", "ob", "ohne", "sage", "sagen", "sagt", "sagte", "sagten", "sagtest", "sehe", "sehen", "sehr", "seht", "sein", "seinem", "seinen", "seiner", "seines", "selbst", "sich", "sicher", "sie", "sind", "so", "solche", "solchem", "solchen", "solcher", "sollte", "sondern", "um", "uns", "unse", "unsen", "unser", "unses", "unter", "viel", "von", "vor", "war", "waren", "warst", "was", "weg", "weil", "weiter", "welchem", "welchen", "welcher", "welches", "welche", "werde", "wieder", "will", "wir", "wird", "wirst", "wo", "wolle", "wollen", "wollt", "wollten", "wolltest", "wolltet", "w�rde", "w�rden", "z.B.", "zum", "zur", "zwar", "zwischen", "�ber", "aber", "abgerufen", "abgerufene", "abgerufener", "abgerufenes", "acht", "allein", "allerdings", "allerlei", "allgemein", "allm�hlich", "allzu", "alsbald", "andererseits", "andernfalls", "anerkannt", "anerkannte", "anerkannter", "anerkanntes", "anfangen", "anfing", "angefangen", "angesetze", "angesetzt", "angesetzten", "angesetzter", "ansetzen", "anstatt", "arbeiten", "aufgeh�rt", "aufgrund", "aufh�ren", "aufh�rte", "aufzusuchen", "ausdr�cken", "ausdr�ckt", "ausdr�ckte", "ausgenommen", "ausser", "ausserdem", "author", "autor", "au�en", "au�er", "au�erdem", "au�erhalb", "bald", "bearbeite", "bearbeiten", "bearbeitete", "bearbeiteten", "bedarf", "bedurfte", "bed�rfen", "befragen", "befragte", "befragten", "befragter", "begann", "beginnen", "begonnen", "behalten", "behielt", "beide", "beiden", "beiderlei", "beides", "beim", "bei", "beinahe", "beitragen", "beitrugen", "bekannt", "bekannte", "bekannter", "bekennen", "benutzt", "bereits", "berichten", "berichtet", "berichtete", "berichteten", "besonders", "besser", "bestehen", "besteht", "betr�chtlich", "bevor", "bez�glich", "bietet", "bisher", "bislang", "bis", "bleiben", "blieb", "bloss", "blo�", "brachte", "brachten", "brauchen", "braucht", "bringen", "br�uchte", "bzw", "b�den", "ca.", "dabei", "dadurch", "daf�r", "dagegen", "dahin", "damals", "danach", "daneben", "dank", "danke", "danken", "dannen", "daran", "darauf", "daraus", "darf", "darfst", "darin", "darum", "darunter", "dar�ber", "dar�berhinaus", "dass", "davon", "davor", "demnach", "denen", "dennoch", "derart", "derartig", "derem", "deren", "derjenige", "derjenigen", "derzeit", "desto", "deswegen", "diejenige", "diesseits", "dinge", "direkt", "direkte", "direkten", "direkter", "doppelt", "dorther", "dorthin", "drauf", "drei", "drei�ig", "drin", "dritte", "drunter", "dr�ber", "dunklen", "durchaus", "durfte", "durften", "d�rfen", "d�rfte", "eben", "ebenfalls", "ebenso", "ehe", "eher", "eigenen", "eigenes", "eigentlich", "einba�n", "einerseits", "einfach", "einf�hren", "einf�hrte", "einf�hrten", "eingesetzt", "einigerma�en", "eins", "einseitig", "einseitige", "einseitigen", "einseitiger", "einst", "einstmals", "einzig", "ende", "entsprechend", "entweder", "erg�nze", "erg�nzen", "erg�nzte", "erg�nzten", "erhalten", "erhielt", "erhielten", "erh�lt", "erneut", "erst", "erste", "ersten", "erster", "er�ffne", "er�ffnen", "er�ffnet", "er�ffnete", "er�ffnetes", "etc", "etliche", "etwa", "fall", "falls", "fand", "fast", "ferner", "finden", "findest", "findet", "folgende", "folgenden", "folgender", "folgendes", "folglich", "fordern", "fordert", "forderte", "forderten", "fortsetzen", "fortsetzt", "fortsetzte", "fortsetzten", "fragte", "frau", "frei", "freie", "freier", "freies", "fuer", "f�nf", "gab", "ganzem", "gar", "gbr", "geb", "geben", "geblieben", "gebracht", "gedurft", "geehrt", "geehrte", "geehrten", "geehrter", "gefallen", "gefiel", "gef�lligst", "gef�llt", "gegeben", "gehabt", "gehen", "geht", "gekommen", "gekonnt", "gemocht", "gem�ss", "genommen", "genug", "gern", "gestern", "gestrige", "getan", "geteilt", "geteilte", "getragen", "gewisserma�en", "geworden", "ggf", "gib", "gibt", "gleich", "gleichwohl", "gleichzeitig", "gl�cklicherweise", "gmbh", "gratulieren", "gratuliert", "gratulierte", "gut", "gute", "guten", "g�ngig", "g�ngige", "g�ngigen", "g�ngiger", "g�ngiges", "g�nzlich", "haette", "halb", "hallo", "hast", "hattest", "hattet", "heraus", "herein", "heute", "heutige", "hiermit", "hiesige", "hinein", "hinten", "hinterher", "hoch", "hundert", "h�tt", "h�tte", "h�tten", "h�chstens", "igitt", "immer", "immerhin", "important", "indessen", "info", "infolge", "innen", "innerhalb", "insofern", "inzwischen", "irgend", "irgendeine", "irgendwas", "irgendwen", "irgendwer", "irgendwie", "irgendwo", "je", "jedenfalls", "jederlei", "jedoch", "jemand", "jenseits", "j�hrig", "j�hrige", "j�hrigen", "j�hriges", "kam", "kannst", "kaum", "keines", "keinerlei", "keineswegs", "klar", "klare", "klaren", "klares", "klein", "kleinen", "kleiner", "kleines", "koennen", "koennt", "koennte", "koennten", "komme", "kommen", "kommt", "konkret", "konkrete", "konkreten", "konkreter", "konkretes", "konnten", "k�nn", "k�nnt", "k�nnten", "k�nftig", "lag", "lagen", "langsam", "lassen", "laut", "lediglich", "leer", "legen", "legte", "legten", "leicht", "leider", "lesen", "letze", "letzten", "letztendlich", "letztens", "letztes", "letztlich", "lichten", "liegt", "liest", "links", "l�ngst", "l�ngstens", "mag", "magst", "mal", "mancherorts", "manchmal", "mann", "margin", "mehr", "mehrere", "meist", "meiste", "meisten", "meta", "mindestens", "mithin", "mochte", "morgen", "morgige", "muessen", "muesst", "musst", "mussten", "mu�", "mu�t", "m�chte", "m�chten", "m�chtest", "m�gen", "m�glich", "m�gliche", "m�glichen", "m�glicher", "m�glicherweise", "m�ssen", "m�sste", "m�ssten", "m��te", "nachdem", "nacher", "nachhinein", "nahm", "nat�rlich", "nacht", "neben", "nebenan", "nehmen", "nein", "neu", "neue", "neuem", "neuen", "neuer", "neues", "neun", "nie", "niemals", "niemand", "nimm", "nimmer", "nimmt", "nirgends", "nirgendwo", "nutzen", "nutzt", "nutzung", "n�chste", "n�mlich", "n�tigenfalls", "n�tzt", "oben", "oberhalb", "obgleich", "obschon", "obwohl", "oft", "per", "pfui", "pl�tzlich", "pro", "reagiere", "reagieren", "reagiert", "reagierte", "rechts", "regelm��ig", "rief", "rund", "sang", "sangen", "schlechter", "schlie�lich", "schnell", "schon", "schreibe", "schreiben", "schreibens", "schreiber", "schwierig", "sch�tzen", "sch�tzt", "sch�tzte", "sch�tzten", "sechs", "sect", "sehrwohl", "sei", "seit", "seitdem", "seite", "seiten", "seither", "selber", "senke", "senken", "senkt", "senkte", "senkten", "setzen", "setzt", "setzte", "setzten", "sicherlich", "sieben", "siebte", "siehe", "sieht", "singen", "singt", "sobald", "soda�", "soeben", "sofern", "sofort", "sog", "sogar", "solange", "solc", "hen", "solch", "sollen", "sollst", "sollt", "sollten", "solltest", "somit", "sonstwo", "sooft", "soviel", "soweit", "sowie", "sowohl", "spielen", "sp�ter", "startet", "startete", "starteten", "statt", "stattdessen", "steht", "steige", "steigen", "steigt", "stets", "stieg", "stiegen", "such", "suchen", "s�mtliche", "tages", "tat", "tats�chlich", "tats�chlichen", "tats�chlicher", "tats�chliches", "tausend", "teile", "teilen", "teilte", "teilten", "titel", "total", "trage", "tragen", "trotzdem", "trug", "tr�gt", "toll", "tun", "tust", "tut", "txt", "t�t", "ueber", "umso", "unbedingt", "ungef�hr", "unm�glich", "unm�gliche", "unm�glichen", "unm�glicher", "unn�tig", "unsem", "unser", "unsere", "unserem", "unseren", "unserer", "unseres", "unten", "unterbrach", "unterbrechen", "unterhalb", "unwichtig", "usw", "vergangen", "vergangene", "vergangener", "vergangenes", "vermag", "vermutlich", "verm�gen", "verrate", "verraten", "verriet", "verrieten", "version", "versorge", "versorgen", "versorgt", "versorgte", "versorgten", "versorgtes", "ver�ffentlichen", "ver�ffentlicher", "ver�ffentlicht", "ver�ffentlichte", "ver�ffentlichten", "ver�ffentlichtes", "viele", "vielen", "vieler", "vieles", "vielleicht", "vielmals", "vier", "vollst�ndig", "voran", "vorbei", "vorgestern", "vorher", "vorne", "vor�ber", "v�llig", "w�hrend", "wachen", "waere", "warum", "weder", "wegen", "weitere", "weiterem", "weiteren", "weiterer", "weiteres", "weiterhin", "wei�", "wem", "wen", "wenig", "wenige", "weniger", "wenigstens", "wenngleich", "wer", "werdet", "weshalb", "wessen", "weswegen", "wichtig", "wieso", "wieviel", "wiewohl", "willst", "wirklich", "wodurch", "wogegen", "woher", "wohin", "wohingegen", "wohl", "wohlweislich", "womit", "woraufhin", "woraus", "worin", "wurde", "wurden", "w�hrenddessen", "w�r", "w�re", "w�ren", "zahlreich", "zehn", "zeitweise", "ziehen", "zieht", "zog", "zogen", "zudem", "zuerst", "zufolge", "zugleich", "zuletzt", "zumal", "zur�ck", "zusammen", "zuviel", "zwanzig", "zwei", "zw�lf", "�hnlich", "�bel", "�berall", "�berallhin", "�berdies", "�bermorgen", "�brig", "�brigens"};
		String newText = "";
		List<String> wordsInText = new ArrayList<String>();
		List<String> newWordsInText = new ArrayList<String>();
		int endIndex = 0;
		
		//Place all words in text into String array.
		while(endIndex <= text.length() && endIndex != -1) {
			int startIndex = endIndex;
			endIndex = text.indexOf(" ", startIndex + 1);
			if(endIndex != -1)
				wordsInText.add(text.substring(startIndex, endIndex));
			else
				wordsInText.add(text.substring(startIndex, text.length() - 1));
		}
		
		//An iterator is used since otherwise no elements can be removed from the list while iterating over it
		Iterator<String> wordsInTextIterator = wordsInText.iterator(); 
		//Remove all stop words from list
		
		//Remove all punctuation from text.
		while(wordsInTextIterator.hasNext()) {
			String word = wordsInTextIterator.next();

			if(word.contains("?"))
				word = word.replace("?", "");
			if(word.contains(","))
				word = word.replace(",", "");
			if(word.contains("."))
				word = word.replace(".", "");
			if(word.contains(";"))
				word = word.replace(";", "");
			if(word.contains(":"))
				word = word.replace(":", "");
			if(word.contains(" "))
				word = word.replace(" ", "");
			wordsInTextIterator.remove();
			newWordsInText.add(word);
		}
		//Add new "unpunctuated" word to the list
		for(String word: newWordsInText) {
			wordsInText.add(word);
		}
		 
		//Finally, remove all stop words from the list
		for(String stopWord: stopList) {
			//System.out.println(stopWord);
			wordsInTextIterator = wordsInText.iterator();
			while(wordsInTextIterator.hasNext()) {
				String word = wordsInTextIterator.next();
				if(word.toLowerCase().equals(stopWord)) {
					//System.out.println(word);
					wordsInTextIterator.remove();
				}
			}
		}
		
		//System.out.println(wordsInText);
		return wordsInText;
	}
	
	/**
	 * Returns a list containing our n-grams as multiple Lists of Strings. 
	 * The number of lists (i.e. n-grams) within this list can be calculated using the equation:
	 * num. of lists/n-grams = X - (n - 1): Where X is the number of words in wordList.
	 * @param n Size of the n-gram we're creating
	 * @param wordList List of words for which we want to create n-grams
	 * @return A list of lists (In reality, a list of n-tuples. n being the size of our n-gram)
	 */
	public List<List<String>> nGramFinder(int n, List<String> wordList) {
		
		List<List<String>> nGramList = new ArrayList<List<String>>(); 
		
		for(int i = 0; i < wordList.size() - (n - 1); i++) {
			
			List<String> nGram = new ArrayList<String>();
			for(int x = 0; x < n; x++) {
				
				nGram.add(wordList.get(i + x).toLowerCase());
				//nGram.add(wordList.get(i + 1));
			}
			
			nGramList.add(nGram);
		}

		return nGramList;
	}
	
	/**
	 * Finds out the number of occurrences of each n-gram in the text. 
	 * @param n Size of the n-gram we're creating
	 * @param nGramList A list of n-gram lists
	 * @return A sorted Map containing the n-grams as keys and how many times they occur as values 
	 */
	public Map<String, Integer> nGramFreq(int n, List<List<String>> nGramList) {
	
		
		//How often do the n-grams appear in the whole text?
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		for(List<String> l: nGramList) {
			String s = "";
			int freq = 0;
			
			for(List<String> l2: nGramList) {
				if(l.equals(l2))
					++freq;
			}
			for(int j = 0; j < n; j++) { 
				s = s + l.get(j) + " ";
				//System.out.print(l.get(j) + " ");
			}
			//System.out.println(s);
			map.put(s, freq);
			
		}
		System.out.println(map);
		return sortByValue(map);
	}
	
	//Sorts a HashMap
	//Source: http://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values-java
	private static <String, Integer> Map<String, Integer> sortByValue(Map<String, Integer> map) {
	    List<Entry<String, Integer>> list = new LinkedList<>(map.entrySet());
	    Collections.sort(list, new Comparator<Object>() {
	        @SuppressWarnings("unchecked")
	        public int compare(Object o1, Object o2) {
	            return ((Comparable<Integer>) ((Map.Entry<String, Integer>) (o1)).getValue()).compareTo(((Map.Entry<String, Integer>) (o2)).getValue());
	        }
	    });

	    Map<String, Integer> result = new LinkedHashMap<>();
	    for (Iterator<Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
	        Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
	        result.put(entry.getKey(), entry.getValue());
	    }

	    return result;
	}
	
	public double logCalc(double num, double base) {
		
		return Math.log(num)/Math.log(base);
	}
}