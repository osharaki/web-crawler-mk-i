package database;

import java.sql.*;

public class SQLiteConnection {

	//private Connection c;
	private String DBpath;
	
	public SQLiteConnection(String DBpath) {
		this.DBpath = DBpath; 
	}
	
	public Connection initiate() {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+DBpath);
			c.setAutoCommit(false);
		} catch (Exception e) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Opened database successfully");
		return c;
	}

	public void readArticleData() {
		Statement stmt;
		ResultSet rs;
		Connection c = initiate();

		if(c != null) {
			try {
				stmt = c.createStatement();
				rs = stmt.executeQuery("select * from articles a, articles b where a.name=b.name AND a.link!=b.link;");
				while ( rs.next() ) {
					//int id = rs.getInt("id");
					String  name = rs.getString("name");
					String category = rs.getString("category");
					String link = rs.getString("link");
					String releaseDate = rs.getString("releaseDate");
					String dateCollected = rs.getString("dateCollected");
					int comments = rs.getInt("comments");
					int shares = rs.getInt("shares");
					int iteration = rs.getInt("iteration");

					System.out.println( "name = " + name );
					System.out.println( "category = " + category );
					System.out.println( "link = " + link );
					System.out.println( "releaseDate = " + releaseDate );
					System.out.println( "dateCollected = " + dateCollected );
					System.out.println( "comments = " + comments );
					System.out.println( "shares = " + shares );
					System.out.println( "iteration = " + iteration );
					System.out.println();
				}
				rs.close();
				stmt.close();
				c.close();
			} catch(Exception e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			}
		}
	}
	
	public void writeArticleData(String name, String category, String link, String releaseDate,
			String dateCollected, int comments, int shares, int iteration) {
		
		Statement stmt;
		Connection c = initiate();
		String query = "INSERT INTO articles (name, category, link, releaseDate, dateCollected, comments, shares, iteration)"
				+ "VALUES ("+name+","+category+","+link+","+releaseDate+","+dateCollected+","+comments+","+shares+","+iteration+")";
		
		if(c != null) {
			
			try {
				stmt = c.createStatement();
				stmt.executeUpdate(query);

				stmt.close();
				c.commit();
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
